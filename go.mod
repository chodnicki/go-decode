module gitlab.com/f5/nginx/controller/libs/go-decode

go 1.16

require (
	github.com/iancoleman/strcase v0.0.0-20190422225806-e506e3ef7365
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337
	github.com/weberr13/go-decode v0.1.5
)
